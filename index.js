const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json);
app.use(bodyParser.urlencoded({extended:true}));
const port = 8000;

//Habilitar servidor
app.listen(port,()=>{
    console.log("Projetos executando na porta : "+ port)
});

//Recurso de consulta
app.get('/funcionario',(req,res)=>{
    console.log("Acessando o recurso FUNCIONARIO");
    res.send("{message:/get funcionario}");
});

app.get('/pesquisa',(req,res)=>{
    let dados = req.query;
    res.send(dados.nome + " " + dados.sobrenome);
});

app.get('/pesquisa/cliente/:codigo',(req,res)=>{
    let id = req.params.codigo;
    res.send("Dados do Cliente " + id);
});

app.post('funcionario/gravar',(req, res)=>{
    let valores = req.body;
    console.log("Nome: " + valores.nome);
    console.log("Sobrenome: " + valores.sobrenome);
    console.log("Idade: " + valores.idade);
    res.send("Sucesso")
});